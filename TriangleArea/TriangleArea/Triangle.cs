﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleArea
{
    public static class Triangle
    {
        public static dynamic TriangleArea(dynamic a, dynamic b)
        {
            if (IsNumeric(a) && IsNumeric(b))
            {
                return a * b / 2;
            }
            else
            {
                throw new ArgumentException("Invalid parameters");
            }
        }
                
        public static bool IsNumeric(ValueType value)
        {
            if (value is Byte ||
                value is SByte ||
                value is Int16 ||
                value is Int32 ||
                value is Int64 ||
                value is UInt16 ||
                value is UInt32 ||
                value is UInt64 ||
                value is Decimal ||
                value is Double ||
                value is Single)
                    return true;
            else
                return false;
        }
    }
}
