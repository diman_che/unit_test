﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriangleArea;
using Microsoft.CSharp;

namespace TriangleTest
{
    [TestClass]
    public class TriangleTest
    {
        [TestMethod]
        public void Area_withValidParam()
        {
            Assert.AreEqual(6, Triangle.TriangleArea(3, 4), "Invalid result, have to be 6");         
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Area_withInvalidValueParameters()
        {
            int a = 4;

            bool b = true;

            Triangle.TriangleArea(a, b);
        }

        [TestMethod]
        [ExpectedException(typeof(Microsoft.CSharp.RuntimeBinder.RuntimeBinderException))]
        public void Area_withInvalidParameters()
        {
            int a = 4;

            string b = "3";

            Triangle.TriangleArea(a, b);
        }
    }
}
